<?php
/**
 * Created by PhpStorm.
 * User: gl
 * Date: 30/12/18
 * Time: 17:56
 */
namespace App\Entity;

use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class filtreArticle{
    /**
     * @var DateTime $startDate
     */
    private $startDate;

    /**
     * @var Date |null
     */
    private $endDate;

    /**
     * @var int|null
     */
    private $minId;

    /**
     * @var int|null
     */
    private $maxId;

    /**
     * @return Date
     */
    public function getStartDate(): Date
    {
        return $this->startDate;
    }

    /**
     * @param Date $startDate
     */
    public function setStartDate(Date $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return Date
     */
    public function getEndDate(): Date
    {
        return $this->endDate;
    }

    /**
     * @param Date $endDate
     */
    public function setEndDate(Date $endDate): void
    {
        $this->endDate = $endDate;
    }



    /**
     * @return int|null
     */
    public function getMinId(): ?int
    {
        return $this->minId;
    }

    /**
     * @param int|null $minId
     */
    public function setMinId(?int $minId): void
    {
        $this->minId = $minId;
    }

    /**
     * @return int|null
     */
    public function getMaxId(): ?int
    {
        return $this->maxId;
    }

    /**
     * @param int|null $maxId
     */
    public function setMaxId(?int $maxId): void
    {
        $this->maxId = $maxId;
    }


}