<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('category',EntityType::class, array(
                'class' => Category::class,
                'choice_label' => 'title'
            ))
//            ->add('category', ChoiceType::class, array(
//                'choices' => array(
//                    'maybe' => null,
//                    'test' => true
//                ),
//                'expanded' => true,
//                'multiple' => true
//            ))
            ->add('content', CKEditorType::class)
            ->add('image')
//            ->add('createdAt', DateType::class, array(
//                'widget' => 'choice',
//                'required' => false
//            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
