<?php

namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;

class PaginationService{

    private $entityClass;
    private $limit = 6;
    private $currentPage = 1;
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function getPages(){
        //1) Connaitre le total des enregistrements de la table
        $repo = $this->manager->getRepository($this->entityClass);
        $total = count($repo->findAll());
        //2) faire la division
        $pages = ceil($total / $this->limit);

        return $pages;
    }

    public  function getData(){
        // 1) calculer l'offset
        $offset = $this->currentPage * $this->limit - $this->limit;

        //2) demande au repo de trouver les éléments
        $repo = $this->manager->getRepository($this->entityClass);
        $data = $repo->findBy([],[], $this->limit, $offset);

        //3) Renvoyer les éléments en questions
        return $data;
    }

    public function getPage()
    {
        return $this->currentPage;
    }


    public function setPage($page)
    {
        $this->currentPage = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * @param mixed $entityClass
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
    }





}