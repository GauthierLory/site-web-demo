<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ArticleFixtures extends Fixture
{

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');
        $manager->persist($adminRole);
        $userRole = new Role();
        $userRole->setTitle('ROLE_USER');
        $manager->persist($userRole);

        $adminUser = new  User();
        $adminUser->setEmail('admin@admin.com')
                    ->setUsername('gauthier')
                    ->setPassword($this->encoder->encodePassword($adminUser,'password'))
                    ->setName('gauthier')
                    ->setLastname('lory')
                    ->addUserRole($adminRole);
        $manager->persist($adminUser);

        // Gestion des Users
        $users = [];
        for ($i = 1; $i <= 10; $i++) {
            $user = new  User();

            $user->setEmail($faker->email)
                ->setUsername($faker->userName)
                ->setPassword($this->encoder->encodePassword($user,'password'))
                ->setName($faker->name)
                ->setLastname($faker->lastName)
                ->addUserRole($userRole);
            $manager->persist($user);
            $users[] = $user;
        }
        // Créer 3 catégories
        for ($i = 0; $i <= 3; $i++) {
            $category = new Category();
            $category->setTitle($faker->sentence())
                ->setDescription($faker->paragraph());

            $manager->persist($category);

            // créer entre 4 et 6 articles par catégories
            for ($j = 1; $j <= mt_rand(4, 6); $j++) {
                $article = new Article();

                $content = '<p>' . join($faker->paragraphs(5), '</p><p>') . '</p>';
                $description= '<p>' . join($faker->paragraphs(1), '</p><p>') . '</p>';

                $user = $users[mt_rand(0, count($users) - 1)];

                $article->setTitle($faker->sentence())
                    ->setDescription($description)
                    ->setContent($content)
                    ->setImage($faker->imageUrl($width = 100, $height = 100))
                    ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                    ->setCategory($category)
                    ->setAuthor($user);

                $manager->persist($article);

                // on donne des commentaires à l'article
                for ($k = 1; $k <= mt_rand(4,10); $k++) {
                    $comment = new Comment();

                    $content = '<p>' . join($faker->paragraphs(3), '</p><p>') . '</p>';

                    $days = (new \DateTime())->diff($article->getCreatedAt())->days;

                    $comment->setAuthor($faker->name)
                        ->setContent($content)
                        ->setCreatedAt($faker->dateTimeBetween('-' . $days . ' days'))
                        ->setArticle($article);

                    $manager->persist($comment);
                }
            }
        }
        $manager->flush();
    }
}
