<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Form\RoleType;
use App\Form\UserRoleType;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\ArticleType;
use App\Service\PaginationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(ArticleRepository $repo,UserRepository $repoUser, Request $request)
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController'
        ]);
    }

    /**
     * @Route("/admin/list/article/{page<\d+>?1}", name="admin_article_list")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function articleList(ArticleRepository $repo, $page, PaginationService $pagination){

        $pagination->setPage($page)
            ->setEntityClass(Article::class);

        return $this->render('admin/articles.html.twig',[
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/admin/user/create", name="admin_user_create")
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function formAdminUser(User $user = null,UserPasswordEncoderInterface $encoder, Request $request, ObjectManager $manager){

        if(!$user){
            $user = new User();
        }

        $formUserRole = $this->createForm(UserRoleType::class, $user);
        $formUserRole->handleRequest($request);

        if ($formUserRole->isSubmitted() && $formUserRole->isValid()){
                $hash = $encoder->encodePassword($user, $user->getPassword());

                $user->setPassword($hash);
                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute('user_list');
            }
        return $this->render('admin/createUser.html.twig',[
            'formUserRole' => $formUserRole->createView(),
            'editMode' => $user->getId() !== null
//            'formUserRoleType' => $formUserRoleType->createView()
        ]);
    }

    /**
     * @Route("/admin/list/user/{page<\d+>?1}", name="admin_user_list")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function usersList($page, PaginationService $pagination){

//        $role = new User();
//        $formUserRole = $this->createForm(UserRoleType::class, $role);
//        $formUserRole->handleRequest($request);
//
//        if ($formUserRole->isSubmitted() && $formUserRole->isValid()){
//            $hash = $encoder->encodePassword($role, $role->getPassword());
//
//            $role->setPassword($hash);
//            $manager->persist($role);
//            $manager->flush();
//        }

        $pagination->setPage($page)
        ->setEntityClass(User::class);

        return $this->render('admin/user.html.twig',[
            'pagination' => $pagination,
//            'formUserRole' => $formUserRole->createView(),
//            'formUserRoleType' => $formUserRoleType->createView()
        ]);
    }

    /**
     * @Route("/admin/new", name="article_create")
     * @Route("admin/{id}/edit", name="article_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function form(Article $article = null,Request $request, ObjectManager $manager){

        if(!$article){
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            if(!$article->getId()){
                $article->setCreatedAt(new  \DateTime());
            }
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show',['id' => $article->getId()]);
        }

        return $this->render('blog/create.html.twig',[
            'formArticle' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * @Route("admin/{id}/delete", name="article_delete", methods="DELETE")
     * @Security("is_granted('ROLE_USER')")
     */
    public function delete(Article $article,ObjectManager $manager, Request $request)
    {
        if($this->isCsrfTokenValid('delete' . $article->getId(), $request->get('_token'))){
            $manager->remove($article);
            $manager->flush();

            $this->addFlash(
                "succes",
                "L annonce a bien été supprimé !"
            );

        }
        return $this->redirectToRoute('admin');
    }
}
