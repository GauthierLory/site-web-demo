<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\filtreArticle;
use App\Form\CommentType;
use App\Form\FiltreArticleType;
use App\Repository\ArticleRepository;
use App\Service\PaginationService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/{page<\d+>?1}", name="blog")
     */
    public function index(PaginationService $pagination, $page,ArticleRepository $repo, Request $request)
    {
        $search = new filtreArticle();
        $formFiltre = $this->createForm(FiltreArticleType::class, $search);

        $formFiltre->handleRequest($request);

        $pagination->setPage($page)
                ->setEntityClass(Article::class);

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'pagination' => $pagination,
            'formFiltre' => $formFiltre->createView()
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(Article $article,Request $request, ObjectManager $manager)
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $comment->setCreatedAt(new \DateTime())
                    ->setArticle($article);

            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('blog_show',['id' => $article->getId()]);
        }

        return $this->render('blog/show.html.twig',[
            'article' => $article,
            'commentForm' => $form->createView()
        ]);
    }

}
