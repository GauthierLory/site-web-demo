<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Indicateur;
use App\Form\IndicateurType;
use App\Repository\UploadRepository;
use App\Service\FileUploader;
use App\Service\PaginationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Contact;
use App\Entity\Upload;
use App\Entity\User;
use App\Form\ContactType;
use App\Form\UploadType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
class FileController extends AbstractController
{
    /**
     * @Route("/file/{page<\d+>?1}", name="file")
     */
    public function index(Request $request, ObjectManager $manager, UploadRepository $repo, $page, FileUploader $fileUploader)
    {
        $limit = 3;
        $start = $page*$limit -$limit;
        $total = count($repo->findAll());
        $pages = ceil($total / $limit); // 3.4 => 4

        $files = $repo->findBy([],[],$limit,$start);

//        $indicateur = new Indicateur();
//        $formIndicateur = $this->createForm(IndicateurType::class, $indicateur);
//        $formIndicateur->handleRequest($request);
//
//        if ($formIndicateur->isSubmitted() && $formIndicateur->isValid()){
//            $manager->persist($indicateur);
//            $manager->flush();
//        }

        $upload = new Upload();
        $formUpload = $this->createForm(UploadType::class, $upload);

        $formUpload->handleRequest($request);

        if($formUpload->isSubmitted() && $formUpload->isValid()){
            $upload->setUploadedAt(new \DateTime);
            $file = $upload->getName();
            $filename = $fileUploader->upload($file);

            $upload->setTitle($file);
            $upload->setName($filename);

            $manager->persist($upload);
            $manager->flush();
        }

        return $this->render('file/index.html.twig', [
            'controller_name' => 'FileController',
            'files' => $files,
            'pages' => $pages,
            'page' => $page,
//            'formIndicateur' => $formIndicateur->createView(),
            'formUpload' => $formUpload->createView()
        ]);
    }

    /**
     * @Route("/file/download/{id}/", name="upload_download")
     */
    public function download(Upload $upload){
        $file = new Upload();

        return $this->file($file);
    }

    /**
     * @Route("/file/upload/{id}/delete", name="upload_delete", methods="DELETE")
     */
    public function delete(Upload $upload,ObjectManager $manager, Request $request)
    {
        if($this->isCsrfTokenValid('delete' . $upload->getId(), $request->get('_token'))){
            $manager->remove($upload);
            $manager->flush();

//            $this->addFlash(
//                'succes',
//                "L'annonce {$upload->getName()}a bien été supprimé !"
//            );

        }
        return $this->redirectToRoute('blog');
    }

}
