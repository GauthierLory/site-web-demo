<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CpaController
 * @package App\Controller
 * @IsGranted("ROLE_CPA")
 */
class CpaController extends AbstractController
{
    /**
     * @Route("/cpa", name="cpa")
     */
    public function index()
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('cpa/index.html.twig', [
            'controller_name' => 'CpaController',
        ]);
    }
}
