<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
                // $product = new Product();
        // $manager->persist($product);
        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 10 ; $i ++){
            $user = new  User();
            $user->setUsername($faker->name)
                ->setPassword($this->passwordEncoder->encodePassword(
                    $user,
                    'userdemo'
                ));
            $manager->persist($user);
        }

        $manager->flush();
    }
}
